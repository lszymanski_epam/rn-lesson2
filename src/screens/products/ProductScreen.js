import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { styles, typography, paddings } from '../../styles'
import { AppButton } from '../../components'


export class ProductScreen extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <View style={localStyles.container}>
        <View style={localStyles.containerLeftHeader}>
          <Image
            style={ localStyles.logo }
            source={ require('../../../assets/drawables/face_happy.png') }/>
          <Text 
            style={ localStyles.header }>
            Product 1
          </Text>
        </View>
        <Text style={ localStyles.description }>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        </Text>
        <View style={ localStyles.containerLeftFooter }>
          <AppButton
            title='All Products'
            onPress={ () => this.props.navigation.goBack() }
            containerStyle={ styles.buttonContainer }
            textStyle={ typography.button }/>
        </View>
      </View>
    )
  }
};

export default ProductScreen

const localStyles = {
  logo: {...styles.logo},
  header: {...typography.textHeader},
  description: {...typography.textRegular, marginRight: paddings.xlarge, marginLeft: paddings.standard, marginBottom: paddings.standard},
  container: {...styles.containerProductDetails},
  containerLeftHeader: {...styles.containerLeft, paddingBottom: paddings.standard },
  containerLeftFooter: {...styles.containerLeft, marginLeft: paddings.standard }
};