import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { colors } from './colors'
import { paddings } from './dimens'


export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: colors.backgroundPrimary,
    paddingLeft: paddings.standard,
    paddingRight: paddings.standard
  },
  containerCenter: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.backgroundPrimary,
    paddingLeft: paddings.standard,
    paddingRight: paddings.standard
  },
  containerLeft: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  buttonContainer: {
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.buttonBackgroundPrimary,
    borderWidth: 1,
    borderColor: colors.borderPrimary,
    borderRadius: 10,
    paddingLeft: paddings.small,
    paddingRight: paddings.small
  },
  containerProductScreen: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  containerProductItem: {
    flex: 1, 
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  containerProductDetails: {
    flex: 1, 
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: colors.backgroundPrimary,
    paddingLeft: paddings.standard,
    paddingRight: paddings.standard
  },
  logo: {
    height: 48,
    width: 48
  },
  image: {
    height: 32,
    width: 32
  },
  textInput: {
    height: 48,
    textAlign: 'center',
    color: colors.textSecondary,
    backgroundColor: colors.backgroundSecondary,
    borderWidth: 1,
    borderColor: colors.borderSecondary,
    borderRadius: 10
  }
});