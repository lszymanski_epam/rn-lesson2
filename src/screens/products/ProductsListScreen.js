import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { styles, typography } from '../../styles'
import { ProductItemView } from '../../components'

export class ProductsListScreen extends Component {
  constructor(props) {
    super(props)
    this.onPress = this.onPress.bind(this);
  }
  
  onPress(iconSource, itemText) {
    this.props.navigation.navigate('Product', {iconSource, itemText})
  }
  
  render() {
    return (
      <View style={ localStyles.container }>
        <Text style={ typography.textHeader }>
          Products
        </Text>
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 1'
          onPressAction={this.onPress} />
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 2' />
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 3' />
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 4' />
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 5'/>
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 6'/>
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 7'/>
        <ProductItemView 
          iconSource={ require('../../../assets/drawables/face_happy.png') }
          itemText='Product 8'/>
      </View>
    )
  }
};

export default ProductsListScreen

const localStyles = {
  container: {...styles.container}
};