import React, { Component } from 'react';
import { View, Text, Button, Image } from 'react-native';
import { styles, typography, paddings } from '../../styles'
import { AppButton, AppTextInput } from '../../components'

export class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerCenter}>
          <Image
            style={screenStyles.logo}
            source={require('../../../assets/drawables/face_happy.png')}/>
        </View>
        <Text style={screenStyles.header}>
          Friday's shop
        </Text>
        <AppTextInput style={screenStyles.email}
          placeholder="email"
          keyboardType="email-address"/>
        <AppTextInput style={screenStyles.password}
          placeholder="Text box"/>
        <View style={styles.containerCenter}>
          <AppButton
            title='login'
            onPress={() => this.props.navigation.navigate('ProductsList')}
            containerStyle={ styles.buttonContainer }
            textStyle={ typography.button }/>
        </View>
      </View>
    )
  }
};

export default LoginScreen;

const screenStyles = {
  logo: {...styles.logo, marginTop: paddings.xlarge},
  header: {...typography.textHeader, marginBottom: paddings.large},
  email: {...typography.textRegular, marginBottom: 24},
  password: {...typography.textRegular, marginBottom: 32}
};
