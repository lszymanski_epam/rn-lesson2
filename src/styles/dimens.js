import React, { Component } from 'react';

export const paddings = {
  small: 24,
  medium: 32,
  large: 48,
  xlarge: 64,
  standard: 32
};

export const fontSizes = {
  regular: 24,
  header: 48
};