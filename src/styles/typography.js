import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { colors } from './colors'
import { fontSizes } from './dimens'

BASE_FONT = 'vincHand';

export const typography = StyleSheet.create({
  textRegular: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.regular,
    color: colors.textPrimary
  },
  textHeader: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.header,
    textAlign: 'center',
    color: colors.textPrimary
  },
  button: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.regular,
    color: colors.buttonTextPrimary
  }
});