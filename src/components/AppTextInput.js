import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import { styles, colors } from '../styles'

export const AppTextInput = ({ style = {}, ...rest }) =>
  <TextInput
    style={ [ styles.textInput, style ] }
    underlineColorAndroid='transparent'
    placeholderTextColor={colors.placeholderSecondary}
    { ...rest } />
