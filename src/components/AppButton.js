import React from 'react';
import {
  Text,
  TouchableOpacity,
  ViewPropTypes
} from 'react-native';
import PropTypes from 'prop-types';

export const AppButton = ({ title, onPress, containerStyle, textStyle }) => {
  return (
    <TouchableOpacity
      activeOpacity = {0.8}
      onPress = { onPress }
      style = { containerStyle }>

      <Text style = { textStyle }>
        { title }
      </Text>
    </TouchableOpacity>
  );
}

AppButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  containerStyle: ViewPropTypes.style,
  textStyle: Text.propTypes.style
};

AppButton.defaultProps = {
  containerStyle: { },
  textStyle: { }
};
