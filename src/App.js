import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { LoginScreen } from './screens/auth'
import { ProductsListScreen, ProductScreen } from './screens/products'

const AppNavigator = createStackNavigator(
  {
    Login: LoginScreen,
    ProductsList: ProductsListScreen,
    Product: ProductScreen
  },
  {
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return (
      <AppContainer />
    );
  }
}